<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Publication;

class PublicationList extends \Widget_BabTableModelView
{
    private $set;
    
    
    public function setDefaultColumns(DynamicRecordSet $set)
    {
        $structure = $set->getStructure();
        
        $this->addColumn(widget_TableModelViewColumn('_content', $structure->getDescription()));
        $this->addColumn(widget_TableModelViewColumn($set->createdOn, translate('Created on')));
        $this->addColumn(widget_TableModelViewColumn('_actions', ''));
        
        
        $I = \bab_functionality::get('Icons');
        if ($I) {
            $I->includeCss();
        }
        
        //$this->addClass(\Func_Icons::ICON_LEFT_16);
        $this->addClass(\Func_Icons::ICON_LEFT_SYMBOLIC);
        
        $this->set = $set;
    }
    
    
    public function computeCellContent(DynamicRecord $record, $fieldPath)
    {
        $W = bab_Widgets();
        $controller = api()->controller()->publication();
        
        switch ($fieldPath) {
            case '_content':
                return $record->getRecordPreview();
                
            case '_actions':
                return $W->FlowItems(
                    $W->Link('', $controller->edit($record->getStructure()->getName(), $record->id))
                        ->addClass('icon')
                        ->addClass(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                        ->setTitle(translate('Edit')),
                    $W->Link('', $controller->deletePublication($record->id, $record->getStructure()->getName()))
                        ->setConfirmationMessage(translate('Do you really want to delete this publication?'))
                        ->addClass('icon')
                        ->addClass(\Func_Icons::ACTIONS_EDIT_DELETE)
                        ->setTitle(translate('Delete'))
                );
        }
        
        return parent::computeCellContent($record, $fieldPath);
    }
    
    
    
    /**
     * Add the filter fields to the filter form
     * @param Widget_Form $form
     *
     */
    protected function handleFilterFields(\Widget_Item $form)
    {
        $W = bab_Widgets();
        $label = $W->Label(translate('Search in all text fields'));
        $lineEdit = $W->LineEdit()->setName('_text')->setAssociatedLabel($label);
        $form->addItem($this->handleFilterLabel($label, $lineEdit));
        
        parent::handleFilterFields($form);
    }
    
    
    public function getFilterCriteria($filter = null)
    {
        
        $criteria = parent::getFilterCriteria($filter);
        
        if (!empty($filter['_text'])) {
            $fields = $this->set->getFields();
            $text = new \ORM_FalseCriterion();
            foreach ($fields as $field) {
                /*@var $field \ORM_Field */
                
                if ('uuid' === $field->getName()) {
                    continue;
                }
                
                switch (get_class($field)) {
                    case 'ORM_StringField':
                    case 'ORM_TextField':
                    case 'ORM_HtmlField':
                        $text = $text->_OR_($field->matchAll($filter['_text']));
                        break;
                }
            }
            
            $criteria = $criteria->_AND_($text);
        }
        
        return $criteria;
    }
}
