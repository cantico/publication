<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 *
 *
 * @method getParentSet DynamicRecordSet
 */
class DynamicRecord extends Dynamic
{

    /**
     * Test if the publication can be sent by email
     * @return boolean
     */
    public function canSendMail()
    {
        $M = \bab_functionality::get('Mailing');
        $ML = \bab_functionality::getOriginal('MailingList');
        if (!$M || !$M->isConfigured() || !$ML || !$ML->canUseMailingList()) {
            return false;
        }

        if (null === $this->getSitemapUrl()) {
            return false;
        }

        return true;
    }

    /**
     * Test if dynamic record is modifiable
     * used by editlinks
     * @return boolean
     */
    public function canUpdate()
    {
        if ($this->pubnode) {
            try {
                return $this->pubnode()->canCreateDynamicRecord();
            } catch (\ORM_Exception $e) {
            }
        }


        $node = $this->getCustomNode();
        if (!isset($node)) {
            // not in sitemap, need to be administrator to edit
            return (bool) bab_isUserAdministrator();
        }

        $sitemapItem = $node->getData();
        /*@var \bab_sitemapItem $sitemapItem */
        return $sitemapItem->canUpdate();
    }


    /**
     * Test if dynamic record is visible
     * @return boolean
     */
    public function canRead()
    {
        if ($this->pubnode) {
            return true;
        }

        $node = $this->getCustomNode();
        if (!isset($node)) {
            // not in sitemap, need to be administrator to edit
            return (bool) bab_isUserAdministrator();
        }

        return true;
    }




    /**
     * Delete draft attached to this dynamic record for a user
     * @param int [$id_user] default is the current logged in user
     */
    public function deleteDraft($id_user = null)
    {
        if (!isset($id_user)) {
            $id_user = bab_getUserId();
        }

        $draftSet = $this->getStructure()->getDraftSet();

        $draftSet->delete(
            $draftSet->dynamicRecord->is($this->id)
            ->_AND_($draftSet->modifiedBy->is($id_user))
        );
    }

    /**
     * Delete modifications history for the dynamic record
     */
    public function deleteHistory()
    {
        $historySet = $this->getStructure()->getHistorySet();

        $historySet->delete(
            $historySet->dynamicRecord->is($this->id)
        );
    }


    public function getNodeId()
    {
        return sprintf('publicationDocument_%s_%d', $this->getStructure()->getName(), $this->id);
    }

    /**
     * Get the url if the publication is in the site sitemap
     */
    public function getSitemapUrl()
    {
        $node = $this->getCustomNode();

        if (!isset($node)) {
            $rootNode = \bab_siteMap::getFromSite();
            if (!isset($rootNode)) {
                bab_debug('No sitemap defined in site settings');
                return null;
            }
            $node = $rootNode->getNodeById($this->getNodeId());

            if (!isset($node)) {
                return null;
            }
        }

        $sitemapItem = $node->getData();
        /*@var $sitemapItem \bab_sitemapItem */

        return $sitemapItem->getRwUrl();
    }


    /**
     *
     * @param string $fieldName
     * @return string
     */
    protected function getTextValue($fieldName)
    {
        $set = $this->getParentSet();
        $text = $this->$fieldName;
        if ($set->$fieldName instanceof \ORM_HtmlField) {
            $text = strip_tags(bab_unhtmlentities($set->$fieldName->output($text)));
        } else {
            $text = $set->$fieldName->output($text);
        }
        return bab_abbr($text, BAB_ABBR_FULL_WORDS, 200);
    }



    /**
     * (non-PHPdoc)
     * @see ORM_Record::getRecordTitle()
     *
     * @return string
     */
    public function getRecordTitle()
    {
        $set = $this->getParentSet();
        $stringFieldName = null;
        $textFieldName = null;

        $fields = $set->getSortedFieldsForPreview();

        foreach ($fields as $field) {
            if (get_class($field) === 'ORM_StringField') {
                $stringFieldName = $field->getName();
                break;
            }

            if ($field instanceof \ORM_TextField && !isset($textFieldName)) {
                $textFieldName = $field->getName();
            }
        }

        if (isset($stringFieldName)) {
            return $set->$stringFieldName->output($this->$stringFieldName);
        }

        if (isset($textFieldName)) {
            return $this->getTextValue($textFieldName);
        }

        return parent::getRecordTitle();
    }


    /**
     *
     * @param string $str
     * @param \bab_url $url
     *
     * @return \Widget_Displayable_Interface
     */
    private function getFirstStringWidget($str, \bab_url $url = null)
    {
        $W = bab_Widgets();

        if (!isset($url)) {
            return $W->Label($str);
        }

        return $W->Link($str, $url);
    }


    /**
     * Get a displayable interface for this record
     *
     * @param \bab_url [$url] optional action for click,
     * use getRecordClickablePreview for a default action
     * @return \Widget_Displayable_Interface
     */
    public function getRecordPreview(\bab_url $url = null)
    {
        $W = bab_Widgets();
        $set = $this->getParentSet();
        $firstString = null;
        $firstStringLabel = null;
        $secondString = null;
        $secondStringLabel = null;
        $image = null;
        $imageLabel = null;

        $fields = $set->getSortedFieldsForPreview();

        foreach ($fields as $field) {
            if (get_class($field) === 'ORM_StringField' && !isset($firstString)) {
                $firstString = $field->output($this->getValue($field->getName()));
                $firstStringLabel = $field->getDescription();
            }

            if ($field instanceof \ORM_TextField && !isset($secondString)) {
                $secondString = $this->getTextValue($field->getName());
                $secondStringLabel = $field->getDescription();
            }

            if ($field instanceof \ORM_FileField && !isset($image)) {
                $filePath = $field->getFilePath($this);
                if ($filePath->fileExists() && false !== getimagesize($filePath->tostring())) {
                    $image = $filePath;
                    $imageLabel = $field->getDescription();
                }
            }
        }

        if (!isset($firstString)) {
            return $this->getFirstStringWidget($this->getRecordTitle(), $url);
        }

        if (isset($firstString) && !isset($secondString) && !isset($image)) {
            return $this->getFirstStringWidget($firstString, $url);
        }

        if (isset($firstString) && isset($secondString)) {
            $vbox = $W->VBoxItems(
                $this->getFirstStringWidget($firstString, $url)
                    ->setTitle($firstStringLabel)
                    ->addClass('widget-strong'),
                $W->Label($secondString)
                    ->setTitle($secondStringLabel)
                    ->addClass('widget-small')
            );

            if (!isset($image)) {
                return $vbox;
            }

            $image = $W->ImageThumbnail($image, $imageLabel)
                    ->setThumbnailSize(48, 48)
                    ->setTitle($imageLabel);

            if (isset($url)) {
                $image = $W->Link($image, $url);
            }

            return $W->HBoxItems(
                $image,
                $vbox
            )->setVerticalAlign('middle')->setHorizontalSpacing(.7, 'em');
        }


        return $W->Label(parent::getRecordTitle());
    }



    /**
     * Get all node instances in custom sitemap
     * @return \bab_Node[]
     */
    public function getCustomNodes()
    {
        $targetId = $this->getNodeId();

        $sitemap = \bab_siteMap::getSiteSitemap();
        $baseNodeId = $sitemap->getVisibleRootNode();
        $nodes = array();


        $customNodes = $sitemap->getRootNode()->getNodesByIndex('target', $targetId);
        foreach ($customNodes as $customNode) {
        /*@var $customNode bab_Node */

            // get the first custom node under baseNode
            $testNode = $customNode->parentNode();
            /*@var $testNode bab_Node */
            do {
                if ($baseNodeId === $testNode->getId()) {
                    $nodes[] = $customNode;
                }

            } while ($testNode = $testNode->parentNode());
        }

        return $nodes;

    }


    /**
     * Get first node instance in custom sitemap
     * @return \bab_Node
     */
    public function getCustomNode()
    {
        $targetId = $this->getNodeId();

        $sitemap = \bab_siteMap::getSiteSitemap();
        $baseNodeId = $sitemap->getVisibleRootNode();
        $rootNode = $sitemap->getRootNode();

        if (!isset($rootNode)) {
            // sitemap failure
            return null;
        }

        $node = $rootNode->getNodeByTargetId($baseNodeId, $targetId);

        if (!isset($node)) {
            return null;
        }

        return $node;
    }

    /**
     * Get a displayable interface for this record
     * with a link to the first instance in custom sitemap
     * @return \Widget_Displayable_Interface
     */
    public function getRecordClickablePreview()
    {
        $node = $this->getCustomNode();

        if (isset($node)) {
            $sitemapItem = $node->getData();
            $url = new \bab_url($sitemapItem->getRwUrl());
        } else {
            $url = new \bab_url();
            $url->tg = 'oml';
            $url->file = 'structures/'.$this->getStructure()->getName().'.ovml';
            $url->publication = $this->id; // this is the parameter given by PortletPublication
            $url->structure = $this->getStructure()->getName(); // this is the parameter given by PortletPublication
        }

        return $this->getRecordPreview($url);
    }









    /**
     * Returns the output values of the record as an associative array suited for ovml.
     * Joined records are returned as sub-arrays.
     *
     * @return array
     */
    public function getOvmlValues()
    {
        $fields     = $this->oParentSet->getFields();
        $values     = array();
        $format     = array();

        foreach ($fields as $fieldName => $field) {
            $ovmlName = 'Publication'.ucwords(strtolower($fieldName));

            switch (true) {
                case $field instanceof \ORM_RecordSet:
                    $values[$ovmlName] = $this->$fieldName->getOvmlValues();
                    break;

                case $field instanceof \ORM_DatetimeField:
                case $field instanceof \ORM_DateField:
                    $values[$ovmlName] = bab_mktime($this->$fieldName);
                    break;

                case $field instanceof \ORM_UserField:
                    $values[$ovmlName] = (int) $this->$fieldName;
                    break;

                case $field instanceof \ORM_FileField:
                    $values[$ovmlName] = $this->getParentSet()->$fieldName->getFilePath($this);
                    break;

                case $field instanceof \ORM_AreaField:
                    $areavalues = $this->getParentSet()->$fieldName->formOutput($this->$fieldName);
                    foreach ($areavalues as $areakey => $areavalue) {
                        $values[$ovmlName.ucwords(strtolower($areakey))] = $areavalue;
                    }
                    break;

                case $field instanceof \ORM_HtmlField:
                    $format[$ovmlName] = \bab_context::HTML;
                    $values[$ovmlName] = $field->output($this->$fieldName);
                    break;

                default:
                    $values[$ovmlName] = $field->output($this->$fieldName);

            }
        }

        return array(
            'values' => $values,
            'format' => $format
        );
    }
}
