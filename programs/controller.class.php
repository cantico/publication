<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Publication;

class Controller extends \bab_Controller
{
    /**
     * Get tg value to use in URL
     * @return string
     */
    protected function getControllerTg()
    {
        return 'addon/publication/main';
    }


    /**
     * (non-PHPdoc)
     * @see bab_Controller::getObjectName()
     */
    protected function getObjectName($classname)
    {
        $prefix = mb_strlen('Ctrl');
        return strtolower(substr($classname, $prefix));
    }
    
    /**
     * @return CtrlStructure
     */
    public function structure($proxy = true)
    {
        require_once dirname(__FILE__) . '/ctrl/structure.ctrl.php';
        return \bab_Controller::nsControllerProxy(__NAMESPACE__, 'CtrlStructure', $proxy);
    }

    /**
     * @return CtrlPublication
     */
    public function publication($proxy = true)
    {
        require_once dirname(__FILE__) . '/ctrl/publication.ctrl.php';
        return \bab_Controller::nsControllerProxy(__NAMESPACE__, 'CtrlPublication', $proxy);
    }
    
    
    /**
     * @return CtrlDownload
     */
    public function download($proxy = true)
    {
        require_once dirname(__FILE__) . '/ctrl/download.ctrl.php';
        return \bab_Controller::nsControllerProxy(__NAMESPACE__, 'CtrlDownload', $proxy);
    }
}
