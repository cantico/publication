<?php
use Ovidentia\Publication;

//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 *
 * This file contain function allways loaded except in unit tests
 *
 */



// @codingStandardsIgnoreStart


/**
 * publication node type in sitemap editor
 */
class Func_SitemapEditorNode_Publication extends Func_SitemapEditorNode
{

    // @codingStandardsIgnoreEnd

    /**
     * (non-PHPdoc)
     * @see bab_functionality::getDescription()
     */
    public function getDescription()
    {
        require_once dirname(__FILE__).'/functions.php';
        return \Ovidentia\Publication\translate('Structured publication');
    }


    /**
     * Test if the content type allow modification of the target node rights in sitemap editor
     * @return boolean
     */
    public function canEditRights()
    {
        return true;
    }


    public function getContentTypes()
    {
        require_once dirname(__FILE__).'/functions.php';
        require_once dirname(__FILE__).'/utilit/xsd.class.php';

        bab_functionality::includeOriginal('Icons');

        $list = array();

        /*
        $list = array(
            'publication_pubnode' => array(
                'description' => \Ovidentia\Publication\translate(
                    'Publication node (manage multiples publication from same data structure in this node)'
                ),
                'className' => Func_Icons::OBJECTS_PUBLICATION_TOPIC
            )
        );
        */

        $xsd = new \Ovidentia\Publication\Xsd();
        foreach ($xsd->getStructures() as $structure) {
            $list['datastructure/'.$structure->getName()] = array(
                'description' => $structure->getDescription(),
                'className' => Func_Icons::OBJECTS_PUBLICATION_ARTICLE
            );
        }

        return $list;
    }


    protected function includeNodeEditor()
    {
        parent::includeNodeEditor();
        require_once dirname(__FILE__).'/functions.php';
        \Ovidentia\Publication\api()->requireFiles();

    }


    /**
     * Get interface to display in node edit mode
     * @param string $contentType
     * @param smed_Node $node
     * @return \Ovidentia\Publication\NodeEditor
     */
    public function getEditor($contentType, smed_Node $node = null)
    {
        $this->includeNodeEditor();

        if ('datastructure/' === mb_substr($contentType, 0, mb_strlen('datastructure/'))) {
            require_once dirname(__FILE__).'/ui/publicationnodeeditor.class.php';
            $editor = new \Ovidentia\Publication\PublicationNodeEditor($contentType, $node);

            $structureName = mb_substr($contentType, mb_strlen('datastructure/'));

            $structure = \Ovidentia\Publication\api()->xsd()->getStructureByName($structureName);
            /* @var $structure \Ovidentia\Publication\DataStructure */

            if (isset($node) && $node->getTargetId()) {
                $explode = explode('_', $node->getTargetId());
                $id = end($explode);
                $values = $structure->getFormValues($id);
                $editor->setValues($values, array('node', $structure->getName()));

                $editor->setHiddenValue('node[target]', $node->getTargetId());
            }
        }


        return $editor;
    }


    /**
     * Before sitemap node is saved
     * The $node['id'] will exists also if the node is not created
     * the node will not be saved if there is a bab_saveException or a bab_accessException
     *
     * @param array &$node         Form posted node
     * @param bool  $nodeExists    True if the node allready exists in sitemap (node modification)
     */
    public function preSave(array &$node, $nodeExists)
    {
        require_once dirname(__FILE__).'/functions.php';
        require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';


        return $this->preSavePublication($node, $nodeExists);

        /*
        if ($node['type'] === 'publication_pubnode') {
            return $this->preSavePubNode($node, $nodeExists); // deprecated

        } else {
            return $this->preSavePublication($node, $nodeExists);
        }
        */
    }



    /**
     * After sitemap node is saved
     * @param smed_Node $node
     * @param array $values         Form posted node
     * @param mixed preSave         Return value of the preSave method
     */
    public function postSave(smed_Node $node, array $values, $preSave)
    {
        $nodeType = $node->getContentType();

        /*
        if ('publication_pubnode' === $nodeType) {
            return parent::postSave($node, $values, $preSave);
        }
        */


        // copy sitemap language to publication

        $language = (string) $node->getLanguage();
        list($structure, $docId) = $this->getPublicationFromNode($nodeType, $node->getTargetId(), true);
        /*@var $structure \Ovidentia\Publication\DataStructure */
        $structure->setSitemapLanguage($docId, $language);

        return parent::postSave($node, $values, $preSave);
    }



    /**
     * Before sitemap node is saved
     * The $node['id'] will exists also if the node is not created
     * the node will not be saved if there is a bab_saveException or a bab_accessException
     *
     * @deprecated
     *
     * @param array &$node         Form posted node
     * @param bool  $nodeExists    True if the node allready exists in sitemap (node modification)
     *//*
    protected function preSavePubNode(Array &$node, $nodeExists)
    {

        // create a pubnode

        $api = \Ovidentia\Publication\api();
        $api->loadOrm();

        $set = $api->pubNodeSet();

        $pubnode = null;

        if ($nodeExists && $node['id']) {
            $pubnode = $set->get($set->nodeid->is($node['id']));
        }

        $structureName = $node['structure'];


        $structure = $api->xsd()->getStructureByName($structureName);
        $structure->updateDatabase();

        if (!isset($pubnode)) {
            $pubnode = $set->newRecord();
        }

        $pubnode->nodeid = $node['id'];
        $pubnode->structure = $structureName;
        $pubnode->save();


        $node['target'] = 'publicationNode_'.$pubnode->id;
        $node['targetchildnodes'] = 1;

        // need a refresh because the target does not exists

        bab_siteMap::clearAll();
    }
    */

    /**
     *
     */
    protected function getPublicationFromNode($nodeType, $target, $nodeExists)
    {
        $api = \Ovidentia\Publication\api();
        /*@var $api Func_Publication */

        $structureName = mb_substr($nodeType, mb_strlen('datastructure/'));

        $structure = $api->xsd()->getStructureByName($structureName);
        $structure->updateDatabase();


        /* @var $structure \Ovidentia\Publication\DataStructure */

        $docId = null;

        if ($nodeExists) {
            if (empty($target)) {
                throw new Exception('The target argument is Missing');
            }

            $targets = explode('_', $target);
            $docId = end($targets);
        }

        return array($structure, $docId);
    }


    /**
     * Before sitemap node is saved
     * The $node['id'] will exists also if the node is not created
     * the node will not be saved if there is a bab_saveException or a bab_accessException
     *
     * @param array &$node         Form posted node
     * @param bool  $nodeExists    True if the node allready exists in sitemap (node modification)
     */
    protected function preSavePublication(array &$node, $nodeExists)
    {

        $api = \Ovidentia\Publication\api();
        /*@var $api Func_Publication */
        $api->loadOrm();

        $currentTarget = null;
        if (isset($node['target'])) {
            $currentTarget = $node['target'];
        }

        list($structure, $docId) = $this->getPublicationFromNode($node['type'], $currentTarget, $nodeExists);
        /* @var $structure \Ovidentia\Publication\DataStructure */
        $structureName = $structure->getName();
        $publication = $structure->saveRecord($node[$structureName], $docId);

        // try to create node[label] if field not set
        if (empty($node['label'])) {
            $structure = $api->xsd()->getStructureByName($structureName);
            $set = $structure->getRecordSet();
            $fields = $set->getSortedFieldsForPreview();

            foreach ($fields as $field) {
                if (get_class($field) === 'ORM_StringField') {
                    $fname = $field->getName();
                    if (!empty($node[$structureName][$fname])) {
                        // Set node label with first string field value
                        $node['label'] = (string) $node[$structureName][$fname];
                        break;
                    }
                }
            }
        }


        $node['target'] = $publication->getNodeId();
        $node['targetchildnodes'] = 1;

        // need a refresh because the target does not exists

        bab_siteMap::clearAll();
    }



    /**
     * Update treeview element before insert into the treeview (set custom icon ... )
     * @param smed_Node             $node
     * @param bab_TreeViewElement   $element
     */
    public function updateTreeViewElement(smed_Node $node, bab_TreeViewElement $element)
    {
        $addonInfos = bab_getAddonInfosInstance('publication');
        $nodeImagePath = $addonInfos->getImagesPath().'nodes/';


        $element->setIcon($nodeImagePath . 'gtk-edit.png');
    }
}
