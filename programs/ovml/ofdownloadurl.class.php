<?php
use Ovidentia\Publication;

//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * Create a download link
 *
 * <OFDownloadUrl path="localPath" [permanent="0"] [inline="1"] saveas="">
 *
 */// @codingStandardsIgnoreStart
class Func_Ovml_Function_DownloadUrl extends \Func_Ovml_Function
{
    // @codingStandardsIgnoreEnd
    
    /**
     *
     * @return string
     */
    public function toString()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
        require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        require_once dirname(__FILE__) . '/../functions.php';
        
        $args = $this->args;

        $inline = 1;
        $path = null;
        $saveas = null;
        $permanent = false;

        if (count($args)) {
            foreach ($args as $p => $v) {
                switch (mb_strtolower(trim($p))) {
                    case 'path':
                        $path = $v;
                        break;
                        
                    case 'inline':
                        $inline = $v;
                        break;
                        
                    case 'saveas':
                        $saveas = $v;
                        break;
                        
                    case 'permanent':
                        $permanent = (bool) $v;
                        break;
                }
            }
        }
        
        $output = '';
        
        if (isset($path)) {
            if ($permanent) {
                $uuid = md5($path);
            } else {
                $uuid = bab_uuid();
            }
            $session = bab_getInstance('bab_Session');
            
            $values = array();
            if (isset($session->ofdownloadurl)) {
                $values = $session->ofdownloadurl;
            }
            $values[$uuid] = $path;
            $session->ofdownloadurl = $values;
            $output = Ovidentia\Publication\api()->controller()->download()->uuid($uuid, $inline)->url();
        }

        if ($saveas) {
            $this->gctx->push($saveas, $output);
            return '';
        }

        return $output;
    }
}
