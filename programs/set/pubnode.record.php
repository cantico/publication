<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 * Instance off data structure
 *
 * @property int $id
 * @property string $structure
 * @property string $nodeid
 * @property string $xsdchecksum
 * @property string $createdOn
 * @property string $modifiedOn
 * @property int $createdBy
 * @property int $modifiedBy
 */
class PubNode extends Record
{
    
    /**
     * @return DataStructure
     */
    public function getStructure()
    {
        return api()->xsd()->getStructureByName($this->structure);
        
    }
    
    
    /**
     * Test if a dynamic record can be created associated to this pubNode
     * used by editlinks
     * @return bool
     */
    public function canCreateDynamicRecord()
    {
        return bab_isUserAdministrator();
    }
}
