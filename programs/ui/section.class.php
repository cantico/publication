<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Publication;

class Section extends \Widget_Section
{
    protected $fset;
    
    protected $structureName;
    
    protected $duplicableField;
    
    /**
     * Prefix path to use in form for the whole publication
     * @var array
     */
    protected $namePath;
    
   /**
    * name path of section in editor
    * @var array
    */
    protected $widgetNamePath;
    
    
    /**
     *
     * @param FieldSet $field
     * @param unknown_type $duplicableField
     *
     */
    public function __construct(FieldSet $field, $duplicableField, $widgetNamePath)
    {
        $this->fset = $field;
        $this->structureName = $field->dataStructure->getName();
        $this->duplicableField = $duplicableField;
        $this->namePath = $field->dataStructure->namePath;
        $this->widgetNamePath = $widgetNamePath;
        
        parent::__construct($field->getLabel(), null, 3);

        $this->setFoldable(true, false);
        
    }
    
    
    
    
    
    
    public function display(\Widget_Canvas $canvas)
    {
        $W = bab_Widgets();
        $controller = api()->controller()->publication();
        
        $this->setReloadAction(
            $controller->reloadFieldSet(
                $this->structureName,
                $this->fset->getElement()->getNodePath(),
                $this->getNamePath()
            )
        );
        
        $addAction = $controller->addSection(
            $this->structureName,
            $this->duplicableField->getElement()->getNodePath(),
            $this->getNamePath()
        );
        
        $vbox = $this->fset->getWidgets($this->widgetNamePath, $this->isDisplayMode());
        
        $this->additem($vbox);
        
        if (!$this->isDisplayMode()) {
            $this->addContextMenu('inline')->addItem(
                $W->Link('', $addAction)
                    ->setAjaxAction($addAction, $vbox)
                    ->addClass('icon', \Func_Icons::ACTIONS_LIST_ADD)
            );

        }
        
        return parent::display($canvas);
    }
}
