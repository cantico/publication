<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 * structure used by publication
 *
 * @property \ORM_StringField       $name                 Structure name
 * @property \ORM_StringField       $path                 Relative path to ovidentia root
 * @property \ORM_DatetimeField     $synchronizedOn       Last database synchronization
 * @property \ORM_IntField          $rows                 Number of rows in structure table
 *
 * @method UsedStructure[]|\ORM_Iterator select(\ORM_Criteria $criteria)
 * @method UsedStructure newRecord()
 * @method UsedStructure get(mixed $criteria)
 */
class UsedStructureSet extends RecordSet
{
    public function __construct()
    {
        parent::__construct();
        
        $this->setDescription('Structures used by publication addon, used to build sitemap');
    
        $this->addFields(
            ORM_StringField('name')
                ->unique()
                ->setDescription('Structure name'),
            ORM_StringField('path')
                ->unique()
                ->setDescription('Relative path to ovidentia root'),
            ORM_DatetimeField('synchronizedOn')
                ->setDescription('Last database synchronization'),
            ORM_IntField('rows')
                ->setDescription('Number of rows in structure table')
        );

    }
    
    /**
     * Remove used structures if not exists
     * The content in table remain but will be invisibles
     *
     */
    public function removeDeletedFiles()
    {
        $res = $this->select();
        $invalid = array();
        
        foreach ($res as $usedStructure) {
            if (!file_exists($usedStructure->path)) {
                $invalid[] = $usedStructure->id;
            }
        }
        
        if ($invalid) {
            $this->delete($this->id->in($invalid));
        }
    }
}
