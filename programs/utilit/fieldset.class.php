<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 *
 */
class FieldSet extends DataStructureField
{

    /**
     * all fields
     * @var array
     */
    protected $fields;



    /**
     * Fields to save in tables, does not include elements with maxOccurs > 0
     * @var array
     */
    protected $dataFields;



    /**
     * Load relative fields from complexType/sequence
     */
    public function loadFields()
    {
        if (!isset($this->fields)) {
            $this->fields = array();
            $this->dataFields = array();

            $xsd = $this->namespacePrefix;
            $relativePath = "./$xsd:complexType/$xsd:sequence/$xsd:element";


            $elements = $this->domXPath->query($relativePath, $this->element);

            if (!isset($elements) || 0 === $elements->length) {
                throw new \Exception(
                    sprintf(
                        translate('The element sequence is empty, from %s'),
                        $this->element->nodeValue
                    )
                );
            }

            foreach ($elements as $node) {
                $subElements = $this->domXPath->query($relativePath, $node);

                if (0 === $subElements->length) {
                    $fieldOrSet = new DataStructureField($node, $this->domXPath, $xsd, $this->dataStructure);

                    $this->fields[] = $fieldOrSet;
                    $this->dataFields[] = $fieldOrSet;

                } else {
                    $fieldOrSet = new FieldSet($node, $this->domXPath, $xsd, $this->dataStructure);
                    $fieldOrSet->loadContent();
                    $fieldOrSet->loadFields();

                    $this->fields[] = $fieldOrSet;

                    if ($this->getSubElementMaxOccurs($subElements) <= 1) {
                        // duplicables fields are not inserted in tables
                        // this is a hasMany relation
                        $this->dataFields[] = $fieldOrSet;
                    }

                }

                $fieldOrSet->parentFieldSet = $this;
            }
        }
    }


    /**
     * @return Number
     */
    protected function getSubElementMaxOccurs(\DOMNodeList $list)
    {
        $maxOccurs = 1;
        if ((1 === $list->length)) {
            $subElement = $list->item(0);
            if (isset($subElement->attributes)) {
                $attribute = $subElement->attributes->getNamedItem('maxOccurs');
                if (isset($attribute) && $attribute->nodeValue) {
                    if ('unbounded' === $attribute->nodeValue) {
                        $maxOccurs = INF;
                    } else {
                        $maxOccurs = (int) $attribute->nodeValue;
                    }
                }
            }
        }

        return $maxOccurs;
    }


    protected function isMultipleNode(\DOMNode $node)
    {
        if ($node->attributes) {
            $maxOccurs = $node->attributes->getNamedItem('maxOccurs');
            if (isset($maxOccurs) && $maxOccurs->nodeValue) {
                if ('unbounded' === $maxOccurs->nodeValue || $maxOccurs->nodeValue > 1) {
                    return true;
                }
            }
        }

        return false;
    }



    /**
     * @return DataStructureField[]
     */
    public function getFields()
    {
        $list = array();

        foreach ($this->fields as $field) {
            if ('Ovidentia\Publication\DataStructureField' === \get_class($field)) {
                $list[] = $field;
            }
        }

        return $list;
    }


    /**
     * @return FieldSet[]
     */
    public function getSubSets()
    {
        $list = array();

        foreach ($this->fields as $field) {
            if ($field instanceof FieldSet) {
                $list[] = $field;
            }
        }

        return $list;
    }




    /**
     * @return DynamicRecordSet
     */
    public function getRecordSet()
    {
        api()->includeDynamicSet();
        $set = new DynamicRecordSet();
        $this->setOrmFields($set);
        $set->setTableName($this->getDynamicRecordSetTableName());

        return $set;
    }


    /**
     * @return DynamicDraftSet
     */
    public function getDraftSet()
    {
        api()->includeDynamicSet();
        $set = new DynamicDraftSet();
        $this->setOrmFields($set);
        $set->setTableName($this->getDynamicDraftSetTableName());
        $set->hasOne('dynamicRecord', '\Ovidentia\Publication\DynamicRecordSet');
        $dynamicRecord = $set->dynamicRecord;
        /*@var $dynamicRecord \ORM_FkField */
        $dynamicRecord->setForeignSet($this->getRecordSet());
        return $set;
    }


    /**
     * @return DynamicHistorySet
     */
    public function getHistorySet()
    {
        api()->includeDynamicSet();
        $set = new DynamicHistorySet();
        $this->setOrmFields($set);
        $set->setTableName($this->getDynamicHistorySetTableName());
        $set->hasOne('dynamicRecord', '\Ovidentia\Publication\DynamicRecordSet');
        $dynamicRecord = $set->dynamicRecord;
        /*@var $dynamicRecord \ORM_FkField */
        $dynamicRecord->setForeignSet($this->getRecordSet());

        return $set;
    }



    /**
     *
     */
    public function setOrmFields(DynamicSet $set)
    {
        $set->setDataStructure($this->dataStructure);

        $set->setDescription($this->getLabel());

        foreach ($this->dataFields as $field) {
            if (!($field instanceof FieldSet)) {
                $set->addFields($field->getOrmField());
            }
        }

        $this->setOrmParentField($set);
    }



    /**
     * Get parent fields set
     * work if xsd has been loaded top to botom with the cached property
     * otherwise, create the fieldSet and load fields and set the cache variable (bottom to top)
     *
     * @return FieldSet|null
     */
    public function getParentFieldSet()
    {
        if (isset($this->parentFieldSet)) {
            return $this->parentFieldSet;
        }

        if (isset($this->element->parentNode->parentNode->parentNode)) {
            $this->parentFieldSet = new FieldSet(
                $this->element->parentNode->parentNode->parentNode,
                $this->domXPath,
                $this->namespacePrefix,
                $this->dataStructure
            );
            $this->parentFieldSet->loadFields();

            return $this->parentFieldSet;
        }

        return null;
    }




    public function setOrmParentField(DynamicSet $set)
    {
        $oneLevelUp = $this->getParentFieldSet();
        if (!isset($oneLevelUp)) {
            return;
        }

        $parentFSet = $oneLevelUp->getParentFieldSet();

        if (!isset($parentFSet)) {
            return;
        }

        $parentName = $parentFSet->getName();
        $parentSet = $parentFSet->getRecordSet();

        foreach ($this->getParentFieldSet()->fields as $field) {
            if ($field instanceof FieldSet && $this->getNamePath() === $field->getNamePath()) {
                $set->hasOne($parentName, get_class($parentSet));
                $set->$parentName->setForeignSet($parentSet);
            }
        }

    }



    /**
     * Get a dupliquable set or a dupliquable field
     * @param FieldSet $field
     * @return DataStructureField
     */
    public function getDuplicableField(FieldSet $field)
    {
        $field->loadFields();

        if (1 === count($field->fields)) {
            $dupli = reset($field->fields);
            /*@var $dupli FieldSet */

            if (1 >= $dupli->maxOccurs) {
                return null;
            }

            return $dupli;
        }

        return null;
    }



    /**
     * Estimate if it's better to have a 2 column layout
     * @return bool
     */
    private function estimateTwoColumnLayout()
    {

        $nbRegular = 0;
        $nbFiles = 0;

        foreach ($this->fields as $field) {
            if ($field instanceof FieldSet) {
                // contain sub-sections, stay on one column
                return false;
            }

            $ormField = $field->getOrmField();

            if ($ormField instanceof \ORM_FileField) {
                $nbFiles++;
            } else {
                $nbRegular++;
            }
        }

        if ($nbFiles > $nbRegular || 0 === $nbFiles || 0 === $nbRegular) {
            return false;
        }

        return true;
    }



    /**
     * Get widget to add in editor for a fieldset
     *
     * @param bool $displayMode     Set fields in displaymode (widget option)
     *
     * @return \Widget_Layout
     */
    public function getWidgets(array $widgetNamePath, $displayMode = false)
    {
        $W = bab_Widgets();
        $W->includePhpClass('Widget_Section');

        require_once dirname(__FILE__).'/../ui/section.class.php';

        $structureName = $this->dataStructure->getName();

        $layout = $W->FlowLayout()->setVerticalSpacing(1, 'em');
        $filesLayout = $W->VBoxLayout()->setVerticalSpacing(2, 'em');

        $controller = api()->controller()->publication();

        $columns = $this->estimateTwoColumnLayout();

        foreach ($this->fields as $field) {
            if ($field instanceof FieldSet) {
                $duplicableField = $this->getDuplicableField($field);

                $duplicableNamePath = $widgetNamePath;
                $duplicableNamePath[] = $field->name;


                if (1 >= $field->maxOccurs && isset($duplicableField)) {
                    // can occur only once and inner field can be duplicated more than once

                    $section = new Section($field, $duplicableField, $duplicableNamePath);
                    $section->setSizePolicy('widget-fullwidth');

                    $layout->addItem($section->setName($field->name));

                } else {
                    // can be duplicated

                    $position = 0;

                    foreach ($field->getSessionOccurs($widgetNamePath) as $name) {
                        $fieldNamePath = $duplicableNamePath;
                        $fieldNamePath[] = $name;

                        $removeAction = $controller->removeSection(
                            $structureName,
                            $field->element->getNodePath(),
                            $position,
                            $fieldNamePath
                        );

                        $frame = $W->Frame(null, $field->getWidgets($fieldNamePath))->setName($name);

                        $section = $W->Section(
                            $field->getLabel().' '.(1+$position),
                            $frame,
                            3
                        )->setFoldable(true, false);

                        $section->setSizePolicy('widget-fullwidth');

                        if (!$displayMode) {
                            $section->addContextMenu('inline')->addItem(
                                $W->Link('', $removeAction)
                                    ->addClass('icon', \Func_Icons::ACTIONS_LIST_REMOVE)
                                    ->setAjaxAction($removeAction)
                            );
                        }


                        $layout->addItem($section->setName($field->name));

                        $layout->sortable();

                        $position++;
                    }

                }



            } else {
                // the labelled widgets will have setSizePolicy('widget-fullwidth') only on text block
                // larger than 100 characters
                // other widget types like selects or checkboxes will be positioned horizontally in the flow

                $labelledWidget = $field->getLabelledWidget();
                $input = $labelledWidget->getInputWidget();

                if ($columns && ($input instanceof \Widget_Uploader || $input instanceof \Widget_ImageCropper)) {
                    $filesLayout->addItem($labelledWidget);
                } else {
                    if (!in_array('widget-fullwidth', $labelledWidget->getInputWidget()->getClasses())) {
                        $layout->addItem(
                            $W->FlowItems($labelledWidget)
                            ->setHorizontalSpacing(3, 'em')
                        );
                    } else {
                        $layout->addItem($labelledWidget);
                    }
                }
            }
        }


        $layout->addItem($W->Hidden()->setName('id'));


        if ($columns) {
            // a right column for attachments ...

            return $W->HBoxItems(
                $layout,
                $filesLayout->setSizePolicy('widget-20em')
            )->setHorizontalSpacing(3, 'em')->addClass('widget-fullwidth');
        }


        return $layout;
    }


    /**
     * Get name path, allways starting with the structure name
     * optionally followed by field names if a sub set
     * this name path is related to the XSD file and not the widget structure
     *
     * @return array
     */
    public function getNamePath()
    {
        $ancestor = $this->element;
        $path = array();

        do {
            if ($ancestor->attributes) {
                $nameAttribute = $ancestor->attributes->getNamedItem('name');

                if ($nameAttribute && $nameAttribute->nodeValue) {
                    array_unshift($path, $nameAttribute->nodeValue);
                }
            }
        } while ($ancestor = $ancestor->parentNode);

        return $path;
    }

    /**
     * For duplicable fields set get the record and the list of ancestors
     * from root to bottom
     * @param DynamicRecord $record
     * @return array
     */
    protected function getRecordHierarchy(DynamicRecord $record, $subRecords = null)
    {
        if (!isset($subRecords)) {
            $subRecords = array();
        }


        $item = new \stdClass();
        $item->fieldSet = $this;
        $item->record = $record;

        array_unshift($subRecords, $item);

        $duplicableFieldSet = $this->getParentFieldSet();

        if (null === $duplicableFieldSet) {
            return $subRecords;
        }

        $item = new \stdClass();
        $item->fieldSet = $duplicableFieldSet;
        $item->record = null;

        array_unshift($subRecords, $item);

        $parentFSet = $duplicableFieldSet->getParentFieldSet();

        if (null === $parentFSet) {
            return $subRecords;
        }

        $fkFieldName = $parentFSet->getName();
        $parentRecord = $record->$fkFieldName();

        return $parentFSet->getRecordHierarchy($parentRecord, $subRecords);
    }


    /**
     * Test if this field set is duplicable in a section widget
     * @return bool
     */
    public function isDuplicable()
    {
        $container = $this->getParentFieldSet();
        if (!isset($container)) {
            return false;
        }

        // container of duplicable element must have a parent
        return (null !== $container->getParentFieldSet());
    }


    /**
     * Get name path used in widget structure, deduced from record and positions
     * @param DynamicRecord $record
     * @return string
     */
    public function getBaseWidgetNamePath(DynamicRecord $record)
    {
        $hierarchy = $this->getRecordHierarchy($record);
        $widgetNamePath = array();

        if (isset($this->dataStructure->namePath)) {
            $widgetNamePath = $this->dataStructure->namePath;
        }

        foreach ($hierarchy as $item) {
            $widgetNamePath[] = $item->fieldSet->getName();

            if ($item->fieldSet->isDuplicable() && isset($item->record)) {
                $widgetNamePath[] = $item->record->sortkey -1;
            }
        }


        return $widgetNamePath;
    }



    /**
     * Table name from path
     * max length for a table name is 64 characters
     * 13 characters are reserved for prefix
     * 32 characters are reserver for md5 of path
     * this leave 19 characters for path information
     * @return string
     */
    private function getTableNameFromPath()
    {
        $path = $this->getNamePath();
        $md5 = md5(implode('', $path));
        $short = array();

        // first element in path is the structure name
        if ('theme_' === mb_substr($path[0], 0, 6)) {
            $path[0] = mb_substr($path[0], 6);
        }

        $max = floor(19/ count($path));

        foreach ($path as $element) {
            $element = str_replace(array('_', '-'), '', $element);
            $short[] = mb_substr($element, 0, $max);
        }

        $tableName = mb_strtolower($md5.implode('', $short));

        return $tableName;
    }



    /**
     * Get table name
     * @return string
     */
    public function getDynamicRecordSetTableName()
    {
        return 'publication_r' . $this->getTableNameFromPath();
    }

    /**
     * Get table name
     * @return string
     */
    public function getDynamicDraftSetTableName()
    {
        return 'publication_d' . $this->getTableNameFromPath();
    }

    /**
     * Get table name
     * @return string
     */
    public function getDynamicHistorySetTableName()
    {
        return 'publication_h' . $this->getTableNameFromPath();
    }



    /**
     * Synchronize database for all tables using this structure
     * Do not call directly, use DataStructure::updateDatabase instead
     *
     * @see DataStructure::updateDatabase()
     *
     */
    public function updateDatabase()
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
        $synchronize = new \bab_synchronizeSql();

        $synchronize->addOrmSet($this->getRecordSet());
        $synchronize->addOrmSet($this->getDraftSet());
        $synchronize->addOrmSet($this->getHistorySet());

        $synchronize->updateDatabase();

        foreach ($this->getSubSets() as $fset) {
            $fset->updateDatabase();
        }
    }


    /**
     * Get form values
     * @param DynamicRecord $record
     * @return array
     */
    public function getFormValues(DynamicRecord $record)
    {
        $rootName = $this->getName();
        $values = $record->getFormOutputValues();


        $baseWidgetNamePath = $this->getBaseWidgetNamePath($record);

        foreach ($this->getSubSets() as $multiSet) {
            $name = $multiSet->getName();
            if (!isset($values[$name])) {
                $values[$name] = array();
            }



            foreach ($multiSet->getSubSets() as $duplicableFSet) {
                $duplicableSet = $duplicableFSet->getRecordSet();
                /*@var $duplicableFSet FieldSet */

                $res = $duplicableSet->select($duplicableSet->$rootName->is($record->id));
                $res->orderAsc($duplicableSet->sortkey);

                $widgetNamePath = $baseWidgetNamePath;
                $widgetNamePath[] = $multiSet->getName();

                $duplicableFSet->setSessionOccurs($res->count(), $widgetNamePath);
                $duplicableName = $duplicableFSet->getName();
                foreach ($res as $subRecord) {
                    $values[$name][$duplicableName][] = $duplicableFSet->getFormValues($subRecord);
                }
            }
        }

        return $values;
    }


    /**
     *
     * @param array $post
     * @param DynamicRecord $record
     */
    public function saveRecord(array $post, DynamicRecord $record)
    {
        $set = $record->getParentSet();

        /*@var $set DynamicRecordSet */

        $modifications = 0;
        foreach ($this->getFields() as $field) {
            $prop = $field->getName();
            $ormField = $set->getField($prop);

            if (!isset($ormField) || !isset($post[$prop])) {
                continue;
            }

            $record->$prop = $ormField->input($post[$prop]);
            $modifications++;
        }

        if ($modifications > 0) {
            $record->save();
        }



        foreach ($this->getSubSets() as $fset) {
            /*@var $fset FieldSet */

            /**
             * This is the container name of the multiple sub record
             * @example diapos
             * @var string
             */
            $prop = $fset->getName();

            if (!isset($post[$prop])) {
                continue;
            }

            foreach ($fset->getSubSets() as $multiField) {
                /*@var $multiField FieldSet */

                /**
                 * This is the element name of the duplicable item
                 * @example diapo
                 * @var string
                 */
                $multiProp = $multiField->getName();

                if (!isset($post[$prop][$multiProp])) {
                    // list of posted elements is not defined
                    // all elements where removed from list
                    // saveMultipleRecords is called with empty array to delete all elements
                    $multiField->saveMultipleRecords(array(), $this->getName(), $record);
                    continue;
                }

                $multiField->saveMultipleRecords($post[$prop][$multiProp], $this->getName(), $record);
            }
        }
    }

    /**
     * Save an array of sub records
     * @param array $posts
     * @param string $parentName
     * @param DynamicRecord $parentRecord
     */
    protected function saveMultipleRecords(array $posts, $parentName, DynamicRecord $parentRecord)
    {
        $set = $this->getRecordSet();
        $savedId = array();

        $sortKey = 1;

        foreach ($posts as $post) {
            if (!empty($post['id'])) {
                $record = $set->get($post['id']);
            }

            if (!isset($record)) {
                $record = $set->newRecord();
            }

            $record->$parentName = $parentRecord->id;

            unset($post['id']);
            $record->sortkey = $sortKey;

            $this->saveRecord($post, $record);

            $savedId[] = $record->id;

            unset($record);

            $sortKey++;
        }

        $set->delete($set->$parentName->is($parentRecord->id)
                ->_AND_($set->id->in($savedId)->_NOT()));

    }







    private function getSession()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = \bab_getInstance('bab_Session');
        if (!isset($session->publication_occurs)) {
            $session->publication_occurs = array();
        }

        return $session;
    }


    private function getDefaultRange()
    {
        $this->loadContent();

        if (!isset($this->minOccurs)) {
            throw new \Exception('minOccurs is required');
        }

        if (0 === $this->minOccurs) {
            return array();
        }

        return range(0, $this->minOccurs -1);
    }



    /**
     * Session path for session occurs
     * @param array [$widgetNamePath]    Name path of the section widget
     * @return string
     */
    protected function getSessionPath(array $widgetNamePath)
    {
        $trace = debug_backtrace();
        $previous = $trace[1];
        bab_debug($previous['function'].' '. implode('/', $widgetNamePath));

        return implode('/', $widgetNamePath);
    }



    /**
     * Set number of occurrences in session variable
     * @param int $count
     */
    public function setSessionOccurs($count, array $widgetNamePath)
    {

        $session = $this->getSession();
        $path = $this->getSessionPath($widgetNamePath);

        $storage = array();
        if (isset($session->publication_occurs)) {
            $storage = $session->publication_occurs;
        }

        if ($count > 0) {
            $storage[$path] = range(0, $count -1);
        } else {
            $storage[$path] = array();
        }

        $session->publication_occurs = $storage;
    }




    /**
     * Add one occurrence in session variable
     * @param array [$widgetNamePath]    Name path of the section widget
     * @throws \bab_SaveErrorException
     *
     */
    public function addSessionOccurs(array $widgetNamePath)
    {
        $session = $this->getSession();


        $path = $this->getSessionPath($widgetNamePath);

        $storage = array();
        if (isset($session->publication_occurs)) {
            $storage = $session->publication_occurs;
        }

        if (!isset($storage[$path])) {
            $storage[$path] = $this->getDefaultRange();
        }

        $this->loadContent();
        $names = $storage[$path];

        if (count($names) >= $this->maxOccurs) {
            throw new \bab_SaveErrorException(
                sprintf(
                    translate('The structure does not allow more than %d items in "%s"'),
                    $this->maxOccurs,
                    $this->getParentFieldSet()->getLabel()
                )
            );
        }

        $max = end($names);
        $names[] = 1 + $max;
        $storage[$path] = $names;

        $session->publication_occurs = $storage;
    }

    /**
     * Remove one occurance in session variable
     *
     * @throws \bab_SaveErrorException
     *
     * @param int $position        Position in array
     * @param array $widgetNamePath   Name path of the section widget
     */
    public function removeSessionOccurs($position, array $widgetNamePath)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = \bab_getInstance('bab_Session');
        if (!isset($session->publication_occurs)) {
            return;
        }

        array_pop($widgetNamePath); // item to remove
        array_pop($widgetNamePath); // this list

        $path = $this->getSessionPath($widgetNamePath);
        $storage = $session->publication_occurs;


        if (!isset($storage[$path])) {
            throw new \bab_SaveErrorException(
                sprintf(
                    translate('The "%s" item has not been deleted because it is not yet saved'),
                    $this->getParentFieldSet()->getLabel()
                )
            );
        }

        $this->loadContent();

        $names = $storage[$path];

        if (count($names) <= $this->minOccurs) {
            throw new \bab_SaveErrorException(
                sprintf(
                    translate('The structure does not allow less than %d items in "%s"'),
                    $this->minOccurs,
                    $this->getParentFieldSet()->getLabel()
                )
            );
        }

        array_splice($names, $position, 1);
        $storage[$path] = $names;

        $session->publication_occurs = $storage;
    }


    /**
     * Get number of occurance stored in session for the path
     * or fallback to the default occurs attribute from the XSD
     *
     * @param array $widgetNamePath     Name path of the section widget
     *
     * @return array
     */
    public function getSessionOccurs(array $widgetNamePath)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = \bab_getInstance('bab_Session');

        if (!isset($session->publication_occurs)) {
            $session->publication_occurs = array();
        }

        $storage = $session->publication_occurs;
        $path = $this->getSessionPath($widgetNamePath);


        if (!isset($storage[$path])) {
            $storage[$path] = $this->getDefaultRange();
        }

        $session->publication_occurs = $storage;

        return $storage[$path];
    }


    /**
     * Set the sitemap language in records identified by $id (in set and sub-sets)
     *
     * @param array $publications   List of publication id
     * @param string $language
     */
    public function setSubSetsLanguage(array $publications, $language)
    {
        $parentName = $this->getName();

        foreach ($this->getSubSets() as $multiSet) {
            foreach ($multiSet->getSubSets() as $duplicableFieldSet) {
                $set = $duplicableFieldSet->getRecordSet();
                $res = $set->select($set->$parentName->in($publications));

                $idList = array();
                foreach ($res as $record) {
                    $idList[] = $record->id;

                    $record->sitemaplang = $language;
                    $record->save();
                }
                $duplicableFieldSet->setSubSetsLanguage($idList, $language);
            }

        }
    }
}
