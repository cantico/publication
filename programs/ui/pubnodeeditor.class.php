<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 * @deprecated
 */
class PubNodeEditor extends \smed_NodeEditor
{
    public function __construct(\smed_Node $node = null)
    {
        
        
        parent::__construct(null, null, $node);
        $this->setHiddenValue('node[type]', 'publication_pubnode');

        $this->generalSection->addItem($this->structure(), 0);
        $this->generalSection->addItem($this->Url(), 0);
        $this->generalSection->addItem($this->Description(), 0);
        $this->generalSection->addItem($this->Label(), 0);


        if (isset($node)) {
            $set = api()->pubNodeSet();
            $pubNode = $set->get($set->nodeid->is($node->getId()));

            if (isset($pubNode)) {
                foreach ($pubNode->getValues() as $prop => $value) {
                    $this->setValue(array('node', $prop), $value);
                }
            }
        }

        $this->setValue('node/url', 'http://');
    }

    /**
     * Select the data structure used to create the pubnode
     */
    protected function structure()
    {
        $W = bab_Widgets();
        $xsd = api()->xsd();
        $select = $W->Select();
        $structures = $xsd->getStructures();

        foreach ($structures as $structure) {
            $select->addOption($structure->getName(), $structure->getDescription());
        }

        return $this->labelledField(
            translate('Data structure'),
            $select,
            'structure'
        );
    }
}
