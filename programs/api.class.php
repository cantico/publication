<?php
use Ovidentia\Publication;

//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 *
 * This file contain function allways loaded except in unit tests
 *
 */



// @codingStandardsIgnoreStart

/**
 * Publication functionality
 * out of namespace to be accessible from other addons
 */
class Func_Publication extends \bab_functionality
{

    // @codingStandardsIgnoreEnd

    private $xsd;



    /**
     * (non-PHPdoc)
     * @see bab_functionality::getDescription()
     */
    public function getDescription()
    {
        return 'Structured publication functionality';
    }


    /**
     * Initialize mysql ORM backend.
     */
    public function loadOrm()
    {
        if (!class_exists('ORM_MySqlRecordSet')) {
            $Orm = \bab_functionality::get('LibOrm');
            /*@var $Orm Func_LibOrm */
            $Orm->initMySql();
        }

        $mysqlbackend = new \ORM_MySqlBackend($GLOBALS['babDB']);
        \ORM_MySqlRecordSet::setBackend($mysqlbackend);

        
        $this->requireFiles();
    }

    /**
     * Used for test, oherwise it is allready loaded by loadOrm method
     */
    public function requireFiles()
    {
        require_once dirname(__FILE__).'/functions.php';
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
        

        require_once dirname(__FILE__).'/set/base.set.php';
        require_once dirname(__FILE__).'/set/base.record.php';

        require_once dirname(__FILE__).'/utilit/xsd.class.php';
        require_once dirname(__FILE__).'/utilit/datastructure.class.php';
        
        \bab_functionality::includeOriginal('Icons');
    }

    /**
     * Include structure event files
     */
    public function includeStructureEvent()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
        require_once dirname(__FILE__).'/utilit/structureevent.class.php';
    }


    /**
     * @return \Ovidentia\Publication\Ui
     */
    public function ui()
    {
        require_once dirname(__FILE__).'/ui/ui.class.php';
        return new \Ovidentia\Publication\Ui();
    }



    /**
     * Include pubnode
     */
    public function includePubNodeSet()
    {
        require_once dirname(__FILE__).'/set/pubnode.set.php';
        require_once dirname(__FILE__).'/set/pubnode.record.php';
    }
    
    /**
     * Include usedstructure
     */
    public function includeUsedStructureSet()
    {
        require_once dirname(__FILE__).'/set/usedstructure.set.php';
        require_once dirname(__FILE__).'/set/usedstructure.record.php';
    }



    /**
     * Include pubnode
     */
    public function includeDynamicSet()
    {
        require_once dirname(__FILE__).'/set/dynamic.set.php';
        require_once dirname(__FILE__).'/set/dynamic.record.php';
        require_once dirname(__FILE__).'/set/dynamicrecord.set.php';
        require_once dirname(__FILE__).'/set/dynamicrecord.record.php';
        require_once dirname(__FILE__).'/set/dynamicdraft.set.php';
        require_once dirname(__FILE__).'/set/dynamicdraft.record.php';
        require_once dirname(__FILE__).'/set/dynamichistory.set.php';
        require_once dirname(__FILE__).'/set/dynamichistory.record.php';
    }


    /**
     * @return \Ovidentia\Publication\PubNodeSet
     */
    public function pubNodeSet()
    {
        $this->loadOrm();
        $this->includePubNodeSet();
        return new \Ovidentia\Publication\PubNodeSet();
    }
    
    
    /**
     * @return \Ovidentia\Publication\UsedStructureSet
     */
    public function usedStructureSet()
    {
        $this->loadOrm();
        $this->includeUsedStructureSet();
        return new \Ovidentia\Publication\UsedStructureSet();
    }


    public function controller()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/controller.class.php';
        require_once dirname(__FILE__).'/controller.class.php';
        return new \Ovidentia\Publication\Controller();
    }


    /**
     * Get the Xsd instance
     */
    public function xsd()
    {
        if (!isset($this->xsd)) {
            require_once dirname(__FILE__).'/functions.php';
            require_once dirname(__FILE__).'/utilit/xsd.class.php';
            $this->xsd = new \Ovidentia\Publication\Xsd();
        }

        return $this->xsd;
    }
}
