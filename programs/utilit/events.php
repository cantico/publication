<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

namespace Ovidentia\Publication;

/**
 * @deprecated
 *//*
function sitemapPubNodesFolder(\bab_eventBeforeSiteMapCreated $event, DataStructure $structure)
{
    $position = array('root', 'DGAll', 'publicationStructures');

    $item = $event->createItem('publicationPubNodes_'.$structure->getName());

    $item->setLabel(sprintf(translate('Publication nodes for %s'), $structure->getName()));
    $item->setDescription($structure->getDescription());
    $item->addIconClassname(\Func_Icons::PLACES_FOLDER_RED);
    $item->setPosition($position);

    $event->addFolder($item);

    sitemapPubNodes($event, $structure);
}
*/

/**
 * @deprecated
 *//*
function sitemapPubNodes(\bab_eventBeforeSiteMapCreated $event, DataStructure $structure)
{


    $position = array('root', 'DGAll', 'publicationStructures', 'publicationPubNodes_'.$structure->getName());


    // all publications nodes by datastructure

    $set = api()->pubNodeSet();
    $res = $set->select($set->readableCriteria()
        ->_AND_($set->structure->is($structure->getName())));


    foreach ($res as $pubNode) {

        $item = $event->createItem('publicationNode_'.$pubNode->id);
        // impossible d'utilise le nom du noeud car cela necessite d'interrogerer le sitemap
        // il faudrait le demander directement a sitemap_editor
        $item->setLink('?tg=oml&file=structures/'.$structure->getName().'.ovml');
        $item->setLabel($pubNode->nodeid);
        $item->addIconClassname(\Func_Icons::PLACES_FOLDER);
        $item->setPosition($position);

        $event->addFunction($item);
    }
}
*/


function sitemapDocumentsFolder(\bab_eventBeforeSiteMapCreated $event, DataStructure $structure)
{
    $position = array('root', 'DGAll', 'publicationStructures');

    try {
        $item = $event->createItem('publicationDocuments_'.$structure->getName());

        $item->setLabel(sprintf(translate('Documents in %s'), $structure->getName()));
        $item->setDescription($structure->getDescription());
        $item->addIconClassname(\Func_Icons::PLACES_FOLDER);
        $item->setPosition($position);

        $event->addFolder($item);

        sitemapDocuments($event, $structure);

    } catch (\Exception $e) {
        bab_debug($e->getMessage());
    }
}


function sitemapDocuments(\bab_eventBeforeSiteMapCreated $event, DataStructure $structure)
{
    $position = array('root', 'DGAll', 'publicationStructures', 'publicationDocuments_'.$structure->getName());

    $structure->updateDatabase();
    $set = $structure->getRecordSet();

    $res = $set->select();


    foreach ($res as $record) {
        /*@var $record DynamicRecord */

        $item = $event->createItem($record->getNodeId());
        $item->setLabel($record->getRecordTitle());
        $item->setLink('?tg=oml&file=structures/'.$structure->getName().'.ovml');
        $item->addIconClassname(\Func_Icons::OBJECTS_PUBLICATION_ARTICLE);
        $item->setPosition($position);

        $event->addFunction($item);
    }
}



function sitemapStructures(\bab_eventBeforeSiteMapCreated $event)
{
    $set = api()->usedStructureSet();
    $set->removeDeletedFiles();
    
    $res = $set->select($set->rows->greaterThan(0));

    foreach ($res as $usedStructure) {
        $structure = $usedStructure->getStructure();

        sitemapDocumentsFolder($event, $structure);
        // sitemapPubNodesFolder($event, $structure); // deprecated
    }
}




function onBeforeSiteMapCreated(\bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__).'/../functions.php';
    \bab_functionality::includeOriginal('Icons');

    $position = array('root', 'DGAll');

    $item = $event->createItem('publicationStructures');

    $item->setLabel(translate('Publications by structures'));
    $item->addIconClassname(\Func_Icons::PLACES_FOLDER);
    $item->setPosition($position);

    $event->addFolder($item);

    sitemapStructures($event);

}



function onSearchRealms(\bab_eventSearchRealms $event)
{
    require_once dirname(__FILE__).'/../functions.php';
    require_once dirname(__FILE__).'/../search/searchrealm.class.php';

    // create on object per structure


    if ($event->isRequested('Ovidentia\Publication\SearchRealm')) {
        $structures = api()->xsd()->getStructures();
        foreach ($structures as $structure) {
            if ($structure->canSearch()) {
                $searchRealm = new SearchRealm();
                $searchRealm->structure = $structure;
                $event->addRealm($searchRealm);
            }
        }
    }
}
