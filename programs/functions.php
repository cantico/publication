<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 *
 * This file contain function allways loaded except in unit tests
 *
 */
namespace Ovidentia\Publication;

/**
 * Translate
 * @param string $str
 */
function translate($str, $str_plurals = null, $number = null)
{
    if ($translate = \bab_functionality::get('Translate/Gettext')) {
    /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('publication');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}




/**
 * @return \Func_Publication
 */
function api()
{
    $api = \bab_functionality::get('Publication');
    $api->loadOrm();

    return $api;
}


/**
 * Generate a pseudo-random UUID according to RFC 4122
 *
 * @return string the new UUID
 */
function uuid()
{
    require_once $GLOBALS['babInstallPath'].'utilit/uuid.php';
    return bab_uuid();
}


/**
 * Warning: currently rights are set to admin
 */
function canEdit()
{
    return \bab_isUserAdministrator();
}
