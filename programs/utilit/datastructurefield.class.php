<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

class DataStructureField
{
    /**
     * Ovidentia specific type
     * @var array
     */
    private $ovidentiaSpecificElements = array(
        'html' => 'html',
        'babfile' => 'babfile',
        'babimage' => 'babimage',
        'area' => 'area'
    );


    /**
     * @var \DOMNode
     */
    protected $element;

    /**
     * @see DataStructureField::loadContent()
     * @var boolean
     */
    private $contentLoaded = false;


    /**
     * @var \DOMXPath
     */
    protected $domXPath;

    /**
     * @var string
     */
    protected $namespacePrefix;


    /**
     * Value from the "name" attribute
     * @var string
     */
    protected $name;

    /**
     * Text node from annotation/documentation node
     * @var string
     */
    protected $label;

    /**
     * Text node from the comment following annotation/documentation node
     * @var string
     */
    protected $description;

    /**
     * Value from simpleType/restriction/minLength
     * @var int
     */
    protected $minLength;

    /**
     * Value from simpleType/restriction/maxLength
     * @var int
     */
    protected $maxLength;

    /**
     * Value from simpleType/restriction/fractionDigits
     * @var int
     */
    protected $fractionDigits = 2;

    /**
     * Value from simpleType/restriction/pattern
     * @var string
     */
    protected $pattern;


    /**
     * Value from "minOccurs" attribute
     * @var int
     */
    protected $minOccurs  = 1;

    /**
     * Value from "maxOccurs" attribute
     * @var int
     */
    protected $maxOccurs  = 1;



    /**
     * Options form annotation/documentation node only usefull in area element
     * @var string
     */
    protected $MaxWidth = 0;//NOT USE FOR NOW
    protected $MaxHeight = 0;//NOT USE FOR NOW
    protected $MinWidth = 0;
    protected $MinHeight = 0;
    protected $Ratio = 0;


    /**
     * Value from the "type" attribute or from the simpleType/restriction[base] node attribute
     * @var string
     */
    protected $type;


    /**
     * Value from the "base" attribute in the restriction tag
     * @var string
     */
    protected $base;

    /**
     * enumeration items from restriction tag
     * @var array
     */
    protected $enumeration;


    /**
     *
     * @var FieldSet
     */
    public $parentFieldSet;


    /**
     *
     * @var DataStructure
     */
    public $dataStructure;

    public function __construct(\DOMNode $element, \DOMXPath $domXPath, $namespacePrefix, DataStructure $dataStructure)
    {
        $this->element = $element;
        $this->domXPath = $domXPath;
        $this->namespacePrefix = $namespacePrefix;
        $this->dataStructure = $dataStructure;
    }


    /**
     *
     * @return \DOMNode
     */
    public function getElement()
    {
        return $this->element;
    }


    /**
     *
     */
    public function getName()
    {
        $this->loadContent();
        return $this->name;
    }

    public function getLabel()
    {
        $this->loadContent();
        return $this->label;
    }

    public function getDescription()
    {
        $this->loadContent();
        return $this->label;
    }


    public function getMaxWidth()
    {
        $this->loadContent();
        return $this->MaxWidth;
    }
    public function getMaxHeight()
    {
        $this->loadContent();
        return $this->MaxHeight;
    }
    public function getMinWidth()
    {
        $this->loadContent();
        return $this->MinWidth;
    }
    public function getMinHeight()
    {
        $this->loadContent();
        return $this->MinHeight;
    }
    public function getRatio()
    {
        $this->loadContent();
        return $this->Ratio;
    }


    /**
     * @return string
     */
    protected function getAttribute($attributeName)
    {
        $nameAttribute = $this->element->attributes->getNamedItem($attributeName);
        if (!isset($nameAttribute)) {
            return null;
        }

        return bab_getStringAccordingToDataBase($nameAttribute->value, 'UTF-8');
    }


    /**
     *
     * @throw \Exception if type not found
     * @return \DOMNode
     */
    protected function getSimpleTypeByName($typeName)
    {
        $xsd = $this->namespacePrefix;
        $list = $this->domXPath->query("/$xsd:schema/$xsd:simpleType[@name=\"$typeName\"]");

        if (!isset($list) || 0 === $list->length) {
            throw new \Exception(sprintf('Type with name %s not found in schema', $typeName));
        }

        foreach ($list as $simpleTypeNode) {
            return $simpleTypeNode;
        }

        return new \DOMNode();
    }


    /**
     * Get raw value attribute (UTF-8)
     */
    protected function getValue(\DOMNode $node)
    {
        $attrValue = $node->attributes->getNamedItem('value');
        if (isset($attrValue)) {
            return $attrValue->value;
        }
    }

    /**
     * Get comment after a node, in db charset
     * if no comment before the next element or if commment is empty, return null
     * @param \DOMNode $node
     * @return string
     */
    protected function getNextComment(\DOMElement $node)
    {
        $comment = $node;
        do {
            $comment = $comment->nextSibling;
            if (!isset($comment) || 'DOMElement' === get_class($comment)) {
                return null;
            }
        } while ('DOMComment' !== get_class($comment));

        $text = trim($comment->textContent);

        if (empty($text)) {
            return null;
        }

        return bab_getStringAccordingToDataBase($text, 'UTF-8');
    }



    protected function loadAnnotation(\DOMElement $annotation)
    {
        $xsd = $this->namespacePrefix;
        $node = $annotation->firstChild;
        do {
            if (!isset($node)) {
                break;
            }

            if ($xsd.':documentation' === $node->nodeName) {
                
                $nodeValue = bab_getStringAccordingToDataBase($node->nodeValue, 'UTF-8');
                
                if (substr($node->nodeValue, 0, 1) == '@' && strpos($node->nodeValue, '=')) {
                    list($name, $value) = explode('=', substr($nodeValue, 1));
                    $this->$name = $value;
                } else {
                    $this->label = $nodeValue;

                    if ($comment = $this->getNextComment($node)) {
                        $this->description = $comment;
                    }
                }
            }



        } while ($node = $node->nextSibling);
    }


    /**
     * Set property if exists as node
     * @param string $propName
     */
    protected function setProperty($node, $propName)
    {
        $xsd = $this->namespacePrefix;

        if ($xsd.':'.$propName !== $node->nodeName) {
            return;
        }

        $this->$propName = $this->getValue($node);
    }


    protected function setEnumeration($node)
    {
        $xsd = $this->namespacePrefix;

        if ($xsd.':enumeration' !== $node->nodeName) {
            return;
        }

        $enumValue = $this->getValue($node);
        if (null !== $enumValue) {
            $text = $this->getNextComment($node);
            $value = bab_getStringAccordingToDataBase($enumValue, 'UTF-8');
            if (!isset($this->enumeration)) {
                $this->enumeration = array();
            }

            if (!isset($text)) {
                $text = $value;
            }

            $this->enumeration[$value] = $text;
        }

    }


    /**
     * Load restriction node
     */
    protected function loadRestriction(\DOMElement $restriction)
    {


        $attrBase = $restriction->attributes->getNamedItem('base');
        if (isset($attrBase)) {
            $this->base = $attrBase->nodeValue;
        }

        $node = $restriction->firstChild;

        do {
            if (!isset($node)) {
                break;
            }

            $this->setProperty($node, 'minLength');
            $this->setProperty($node, 'maxLength');
            $this->setProperty($node, 'fractionDigits');
            $this->setProperty($node, 'pattern');
            $this->setEnumeration($node);

        } while ($node = $node->nextSibling);
    }


    /**
     * Load the simpleType bloc
     * @property \DOMNode $simpleType
     */
    protected function loadSimpleType(\DOMNode $simpleType)
    {
        $xsd = $this->namespacePrefix;
        $node = $simpleType->firstChild;
        do {
            if (!isset($node)) {
                break;
            }

            if ($xsd.':restriction' === $node->nodeName) {
                $this->loadRestriction($node);
            }

        } while ($node = $node->nextSibling);
    }


    /**
     * Load content from subnodes and attributes
     */
    public function loadContent()
    {
        if ($this->contentLoaded) {
            return;
        }

        $xsd = $this->namespacePrefix;

        $this->name = $this->getAttribute('name');
        $this->label = $this->name; // default value

        if (!isset($this->name)) {
            throw new \Exception('The name attribute is mandatory');
        }

        if (null !== $minOccurs = $this->getAttribute('minOccurs')) {
            $this->minOccurs = (int) $minOccurs;
        }


        if (null !== $maxOccurs = $this->getAttribute('maxOccurs')) {
            if ('unbounded' === $maxOccurs) {
                $this->maxOccurs = INF;
            } else {
                $this->maxOccurs = (int) $maxOccurs;
            }
        }

        if (null !== $type = $this->getAttribute('type')) {
            $this->type = $type;
            if ($this->namespacePrefix.':' !== mb_substr($type, 0, 1+mb_strlen($this->namespacePrefix))) {
                $this->loadSimpleType($this->getSimpleTypeByName($type));
            }
            if (isset($this->ovidentiaSpecificElements[$type])) {
                $this->base = $type;
            }
        }


        $node = $this->element->firstChild;
        do {
            if (!isset($node)) {
                break;
            }

            if ($xsd.':annotation' === $node->nodeName) {
                $this->loadAnnotation($node);
            }

            if ($xsd.':simpleType' === $node->nodeName) {
                // if we have a simpleType, $this->type shound not be defined
                if (isset($this->type)) {
                    $message = sprintf(
                        translate('Type is provided by the subnode "simpleType"').' '.
                        translate('the type attribute is not necessary on %s'),
                        $this->name
                    );

                    throw new \Exception($message);
                }

                $this->loadSimpleType($node);
            }

        } while ($node = $node->nextSibling);

        $this->contentLoaded = true;
    }


    /**
     * Convert unmodified xsd type to ORM field
     * @param string $type
     * @return \ORM_Field
     */
    protected function getBasicXsdField($type)
    {
        $types = explode(':', $type);
        if (isset($types[1])) {
            $type = $types[1];
        } else {
            $type = $types[0];
        }

        switch ($type) {
            case 'normalizedString':
            case 'duration':
                if (!isset($this->maxLength)) {
                    $this->maxLength = 255;
                }

                $stringField = \ORM_StringField($this->name, $this->maxLength);
                if (isset($this->pattern)) {
                    $pattern = new \ORM_CustomProperty('pattern', $this->pattern);
                    $stringField->setCustomPropertiesObject(new \ORM_CustomProperties(array($pattern)));
                }
                return $stringField;

            case 'html':
                return \ORM_HtmlField($this->name);
            case 'string':
            case 'token':
                return \ORM_TextField($this->name);

            case 'dateTime':
                return \ORM_DatetimeField($this->name);

            case 'date':
                return \ORM_DateField($this->name);

            case 'time':
                return \ORM_TimeField($this->name);

            case 'byte':
            case 'int':
            case 'integer':
            case 'negativeInteger':
            case 'nonNegativeInteger':
            case 'nonPositiveInteger':
            case 'positiveInteger':
            case 'short':
            case 'unsignedLong':
            case 'unsignedInt':
            case 'unsignedShort':
            case 'unsignedByte':
                return \ORM_IntField($this->name);

            case 'double':
            case 'float':
            case 'decimal':
                return \ORM_DecimalField($this->name, $this->fractionDigits);

            case 'anyURI':
                return \ORM_UrlField($this->name);

            case 'babfile':
                return \ORM_FileField($this->name);

            case 'babimage':
                $field = \ORM_ImageField($this->name);

                if (isset($GLOBALS['AssociatedAreaField']) && $GLOBALS['AssociatedAreaField']) {//@TODO ugly
                    $field->setAssociatedField($GLOBALS['AssociatedAreaField']);
                    $GLOBALS['AssociatedAreaField'] = null;
                } else {
                    $GLOBALS['AssociatedFileField'] = $field;
                }

                return $field;

            case 'area':
                $field = \ORM_AreaField($this->name);

                /*if(isset($this->MaxWidth) && isset($this->MaxHeight)) {
                    $field->setMaxSize($this->MaxWidth, $this->MaxHeight);
                }*/
                if (isset($this->MinWidth) && isset($this->MinHeight)) {
                    $field->setMinSize($this->MinWidth, $this->MinHeight);
                }

                if (isset($this->Ratio)) {
                    $field->setRatio($this->Ratio);
                }

                if (isset($GLOBALS['AssociatedFileField']) && $GLOBALS['AssociatedFileField']) {//@TODO ugly
                    $field->setAssociatedField($GLOBALS['AssociatedFileField']);
                    $GLOBALS['AssociatedFileField'] = null;
                } else {
                    $GLOBALS['AssociatedAreaField'] = $field;
                }

                return $field;

            case 'babsitemap':
                return \ORM_SitemapItemField($this->name);
            case 'hexBinary':
            case 'base64Binary':
                //TODO binary
                return;

            case 'boolean':
                return \ORM_BoolField($this->name);

        }

        return null;
    }


    /**
     * Convert xsd type from a simpleType tag to a ORM_Field
     * @return \ORM_Field
     */
    protected function getSimpleTypeField()
    {
        if (isset($this->enumeration)) {
            return \ORM_EnumField($this->name, $this->enumeration);
        }

        if (!empty($this->base)) {
            return $this->getBasicXsdField($this->base);
        }

        return null;
    }



    /**
     * @return \ORM_Field
     */
    public function getOrmField()
    {
        $this->loadContent();

        $field = $this->getSimpleTypeField();

        if (!isset($field)) {
            if (empty($this->type)) {
                throw new \Exception(sprintf('The field %s contain no simpleType and no type attribute', $this->name));
            }

            $field = $this->getBasicXsdField($this->type);
        }

        $field->setDescription($this->label);

        return $field;
    }


    /**
     * @return \Widget_LabelledWidget
     */
    public function getLabelledWidget()
    {
        $field = $this->getOrmField();
        $labelledWidget = $field->getLabelledWidget();
        $input = $labelledWidget->getInputWidget();
        if (isset($this->minLength) && $this->minLength > 0) {
            $input->setMandatory(
                true,
                sprintf(
                    translate('%s is mandatory'),
                    $labelledWidget->getLabelText()
                )
            );
        }

        switch (true) {
            case $input instanceof \Widget_FilePicker:
                // do not remove tmp file for ajax refresh in sub fields set
                // do not remove tmp file if failed submit
                $input->clearTemporary($this->dataStructure->getClearTemporary());
                break;

            case $input instanceof \Widget_LineEdit:
                if ($input->getMaxSize() < 100) {
                    $input->setSize($input->getMaxSize());
                    break;
                }
                // fall-through for fullwith class
            case $input instanceof \Widget_TextEdit:
                $input->addClass('widget-fullwidth');
                $labelledWidget->setSizePolicy('widget-fullwidth');
        }

        $labelledWidget->setDescription($this->description);

        return $labelledWidget;
    }
}
