<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
namespace Ovidentia\Publication;

/**
 * Not in database, common fields from dynamicrecord, dynamicdraft and dynamichistory
 *
 * @see Dynamic
 *
 *
 * @property \ORM_StringField $uuid
 * @property \ORM_IntField $sortkey
 * @property \ORM_StringField $sitemaplang
 * @property PubNodeSet $pubnode
 */
abstract class DynamicSet extends RecordSet
{
    
    /**
     * @var DataStructure
     */
    protected $dataStructure;
    
    
    public function __construct()
    {
        parent::__construct();
        
        $this->addFields(
            ORM_IntField('sortkey'),
            ORM_StringField('uuid'),
            ORM_StringField('sitemaplang')
                ->setDescription('publication content laguage from last save in sitemap context')
        );
        
        $this->hasOne('pubnode', '\Ovidentia\Publication\PubNodeSet');
    }
    
    /**
     * Set datastructure and add fields from datastructure
     * @param DataStructure $dataStructure
     */
    public function setDataStructure(DataStructure $dataStructure)
    {
        $this->dataStructure = $dataStructure;
    }

    
    
    /**
     * @return DataStructure
     */
    public function getStructure()
    {
        return $this->dataStructure;
    }
    
    protected function newUsedStructure(UsedStructureSet $set)
    {
        $record = $set->newRecord();
        $record->synchronizedOn = date('Y-m-d H:i:s');
        $record->name = $this->dataStructure->getName();
        $record->path = $this->dataStructure->getPath()->tostring();
        
        
        return $record;
    }
    
    
    /**
     * Update used structure
     */
    protected function updateRows()
    {
        if (!isset($this->dataStructure)) {
            return;
        }
        
        $fset = $this->dataStructure->getSequence(); // main fields set
        if ($this->getTableName() !== $fset->getDynamicRecordSetTableName()) {
            // update rows only with main table
            return;
        }
        
        $set = api()->usedStructureSet();
        $record = $set->get($set->name->is($this->dataStructure->getName()));
    
        if (!isset($record)) {
            $record = $this->newUsedStructure($set);
        }
    
        $record->rows = $this->select()->count();
    
        try {
            $record->save();
        } catch (\ORM_BackEndSaveException $e) {
            // if record allready exists
        }
    
    }
    
    

    public function save(Dynamic $record)
    {
        
        if (empty($record->uuid)) {
            $record->uuid = uuid();
        }
        
    
        $result = parent::save($record);
        
        $this->updateRows();
        
        return $result;
    }
    
    

    
    
    public function delete(\ORM_Criteria $criteria)
    {
        $result = parent::delete($criteria);
        
        $this->updateRows();
        
        return $result;
    }
}
