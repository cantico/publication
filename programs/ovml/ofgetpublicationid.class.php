<?php
use Ovidentia\Publication;

//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2009 by CANTICO ({@link http://www.cantico.fr})
 */

/**
 * Return pubnode id of the current page
 * or document id of the current page
 * or list of document if of the page and sub-pages if all="1"
 *
 * <OFGetPublicationId [node=""] [saveas=""] [all="0"]>
 *
 */// @codingStandardsIgnoreStart
class Func_Ovml_Function_GetPublicationId extends \Func_Ovml_Function
{
    // @codingStandardsIgnoreEnd

    /**
     *
     * @return string
     */
    public function toString()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/sitemap.php';
        require_once dirname(__FILE__) . '/../functions.php';

        $args = $this->args;

        $all = false;
        $currentNode = bab_siteMap::getPosition();
        $saveas = null;

        if (count($args)) {
            foreach ($args as $p => $v) {
                switch (mb_strtolower(trim($p))) {
                    case 'all':
                        $all = (bool) $v;
                        break;

                    case 'node':
                        $currentNode = $v;
                        break;

                    case 'saveas':
                        $saveas = $v;
                        break;
                }
            }
        }


        if ($siteSitemap = bab_siteMap::getFromSite()) {
            $currentNode = $siteSitemap->getNodeById($currentNode);
        }

        if (!$currentNode) {
            if ($saveas) {
                $this->gctx->push($saveas, '');
            }

            return '';
        }

        if ($all) {
            $nodes = $this->getSubNodes($currentNode);
        } else {
            $nodes = array($this->getTarget($currentNode)->getId());
        }


        $output = implode(',', $this->getPublicationIdFromNodes($nodes));


        if ($saveas) {
            $this->gctx->push($saveas, $output);
            return '';
        }

        return $output;
    }



    /**
     * @param bab_Node $nodeId
     * @return array
     */
    protected function getSubNodes(bab_Node $node)
    {
        $nodes = array();
        if ($node->hasChildNodes()) {
            $I = new bab_NodeIterator($node);
            while ($node = $I->nextNode()) {
                $nodes[] = $this->getTarget($node)->getId();

            }
        }

        if (isset($node)) {
            $nodes[] = $this->getTarget($node)->getId();
        }


        return $nodes;
    }


    /**
     * Get publication or pubnodes, do mix in same array
     * @return array
     */
    protected function getPublicationIdFromNodes(array $nodes)
    {
        $return = array();
        $mixText = null;



        foreach ($nodes as $nodeId) {
            if ($this->matchPrefix('publicationNode_', $nodeId)) {
                if ($mixText === 'doc') {
                    continue;
                }

                $return[] = $this->getPubNode($nodeId);
                $mixText = 'pubnode';
                continue;
            }

            if ($this->matchPrefix('publicationDocument_', $nodeId)) {
                if ($mixText === 'pubnode') {
                    continue;
                }

                $return[] = end(explode('_', $nodeId));
                $mixText = 'doc';
                continue;
            }
        }

        return $return;
    }


    protected function matchPrefix($prefix, $nodeId)
    {

        return ($prefix === mb_substr($nodeId, 0, mb_strlen($prefix)));
    }


    /**
     * @return bab_Node
     */
    protected function getTarget(bab_Node $node)
    {
        $sitemapItem = $node->getData();

        $targetSitemapItem = $sitemapItem->getTarget();

        return $targetSitemapItem->node;
    }



    protected function getPubNode($currentNode)
    {
        $api = \Ovidentia\Publication\api();
        $set = $api->pubNodeSet();
        $pubnode = $set->get($set->nodeid->is($currentNode));


        return $pubnode->id;
    }
}



// @codingStandardsIgnoreStart

/**
 *
 * @deprecated use OFGetPublicationId
 */
class Func_Ovml_Function_PublicationNode extends \Func_Ovml_Function_GetPublicationId
{
    
}
// @codingStandardsIgnoreEnd